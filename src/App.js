import React, { Component } from 'react';
import './pages/editor'
import './css/bootstrap.min.css';
import './App.css';
import Editor from './pages/editor';
import Preview from './pages/preview';
import Home from './pages/home';
import Nav from './navigation/nav';
import Blog from './pages/blog';
import Single from './pages/single';
import Login from './pages/login';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { ProtectedRoute } from './routes/protected.route';

class App extends Component {

  render() {
    return(
      <div>
      <Router>
      <Nav />
        <div className="app-wrapper container" id="App">
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/login" exact component={Login} />
            <Route path="/editor" exact component={Editor} />
            <Route path="/blog" exact component={Blog} />
            <Route path="/blog/:id" exact component={Single} />
          </Switch>
        </div>
      </Router>
      </div>
    )
  }
  
}

export default App;
