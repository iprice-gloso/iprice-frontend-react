import React from 'react';
import parser, { Tag } from 'bbcode-to-react';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { docco } from 'react-syntax-highlighter/dist/esm/styles/hljs';

class CodeTag extends Tag {

    decodeHtml(html) {
        let txt = document.createElement('textarea');
        txt.innerHTML = html;
        return txt.value;
    }

    toReact() {
        const attrs = {
            content: this.getContent(true),
            language: this.params.language
        }
        return(
            <SyntaxHighlighter language={this.params.language} style={docco}>
                {this.decodeHtml(attrs.content)}
            </SyntaxHighlighter>
        // <pre className={"prism-code language-" + this.params.language}><code>{this.decodeHtml(attrs.content)}</code></pre>
        )
    }
}

parser.registerTag('code', CodeTag);

export default CodeTag;