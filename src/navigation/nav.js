import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class Nav extends Component {
    constructor(props) {
        super(props);
        this.navStyle = {
            color: 'white',
            marginRight: '24px'
        }
    }
    render() {
        return(
            <div className="nav-wrapper">
                <nav className="navbar navbar-expand-sm bg-primary navbar-dark">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <Link style={this.navStyle} to="/">
                            Home
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link style={this.navStyle} to="/editor">
                            Editor
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link style={this.navStyle} to="/blog">
                            Blog
                        </Link>
                    </li>
                </ul>
                </nav>
            </div>
        )
    }
}

export default Nav;