import React, { Component } from 'react';
import { Link } from 'react-router-dom'

class Blog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            posts: []
        };
    }

    componentDidMount() {
        fetch('http://localhost:4000/blog', {
                method: 'GET',
                mode: 'cors'
        })
        .then(res => res.json())
        .then(
            (res) => {
            this.setState({
                isLoaded: true,
                posts: res
            });
            console.log(res);
        },
        (error) => {
            this.setState({
                isLoaded: true,
                error
            });
        });
    }

    componentDidUpdate() {
        console.log(this.state);
    }
    

    render() {
        const { error, isLoaded, posts } = this.state;
        if(error) {
        return <div>Error: {error.message}</div>
        }
        return(
            <div className="posts">
                {posts.map(post => (
            <h1 key={post._id}>
                <Link to={`/blog/${post._id}`}>{post.title}</Link>
            </h1>
                ))}
            </div>
        )
    }
}

export default Blog;