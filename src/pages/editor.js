import React, { Component } from 'react';
import parser, { Tag } from 'bbcode-to-react';
// import { renderToString } from 'react-dom/server';
// import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import CodeTag from '../bbTags/codetag';
import '../css/bootstrap.min.css';
import '../App.css';
import checkAuth from '../auth/checkAuth';

class Editor extends Component {
    constructor(props) {
      super(props);
      this.txtArea = React.createRef();
      this.wrap = this.wrap.bind(this);
      this.state = {
        txtAreaVal: '',
        needsConverted: '',
        user: undefined
      }
      this.setPreview = this.setPreview.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    insert(str, index, value) {
      return str.substr(0, index) + value + str.substr(index);
    }
  
    wrap(event) {
      let openTag = "[" + event.target.value.toLowerCase() + "]";
      let closedTag = "[/" + event.target.value.toLowerCase() + "]";
      if(event.target.value == 'Code') {
        const enteredLang = prompt('Please enter the programming language');
        openTag = '[code language="' + enteredLang + '"]';
        console.log(openTag);
        
      }
      let toAdd = openTag.length;
      let cursorStart = this.txtArea.current.selectionStart;
      let cursorEnd = this.txtArea.current.selectionEnd;
      let txtVal = this.txtArea.current.value;
  
      let opened = this.insert(txtVal, cursorStart, openTag);
      let closed = this.insert(opened, cursorEnd+toAdd, closedTag);
  
      this.txtArea.current.value = closed;
  
      this.setState({txtAreaVal: closed});
  
    }

    componentDidMount() {
        const check = checkAuth.isAuthenticated();
        console.log(check);
        check.then(res => {
            console.log(res);
            if(res.authenticated === false) {
                this.props.history.push("/login");
                console.log(res.authenticated)
            } else {
                this.setState({ user: res.authenticated })
            }
        })
        .catch(err => { 
            console.error(err);
            localStorage.removeItem("authentication");
        })
    }
  
    setPreview() {
      let needsConverted = this.state.txtAreaVal;
      this.setState({preview: needsConverted});
    }
  
    handleSubmit = (event) => {
      event.preventDefault();
      const data = new FormData(event.target);
      const body = event.target.postBody.value;
      const postTitle = event.target.title.value;
  
      console.log(event.target);
      console.log(data);
      console.log(event.target.postBody.value);
  
      fetch('http://localhost:4000/blog', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          "Authorization": localStorage.getItem('authorization')
        },
        mode: 'cors',
        body: JSON.stringify({
          title: postTitle,
          postBody: body
        })
      })
      .then(res => res.json())
      .catch(err => console.error(err))
    }
  
    render() {

        if(this.state.user === undefined) {
            return(
                <div><h1>Loading...</h1></div>
            )
        }

      const output = this.state.txtAreaVal
      return(
          <div className="editor" id="Editor">
          <h1>Welcome to ReactBB</h1>
          <div className="form-group">
            <input type="button" className="btn btn-secondary" onClick={this.wrap} value="Code" />
            <input type="button" className="btn btn-secondary" onClick={this.wrap} value="H1" />
            <input type="button" className="btn btn-secondary" onClick={this.wrap} value="B" />
            <input type="button" className="btn btn-secondary" onClick={this.wrap} value="I" />
          </div>
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label htmlFor="title"><strong>Title Your Blog Post</strong></label>
            </div>
            <div className="form-group">
            <input className="form-control" id="title" name="title" type="text" />
            </div>
            <div className="form-group">
              <label htmlFor="postBody"><strong>Write Your Blog Post</strong></label>
            </div>
            <div className="form-group">
              <textarea 
              id="postBody" 
              name="postBody"
              className="form-control" 
              defaultValue="" 
              onChange={(event) => {
                this.setState({
                  txtAreaVal: event.target.value
                })
              }} 
              ref={this.txtArea} 
              ></textarea>
            </div>
            <div className="form-group">
              <button className="btn btn-primary" type="submit">Post Blog</button>
            </div>
          </form>
          
          <div className="preview">
            {parser.toReact(output)}
          </div>
        </div>
        
      )
    }
    
  }

export default Editor;