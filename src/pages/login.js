import React, { Component } from 'react'
import checkAuth from '../auth/checkAuth';

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: null,
            password: null,
            isAuthenticated: null
        }

    }

    componentDidMount() {
        //console.log(checkAuth.checkLogin())
        const check = checkAuth.isAuthenticated();
        console.log(check);
        check.then(res => {
            console.log(res);
            if(res.authenticated === true) {
                this.props.history.push("/");
                console.log(res.authenticated)
            }
        })
        .catch(err => { console.error(err) })
    }

    handleSubmit(event) {
        event.preventDefault();
        const formData = {
            username: event.target.username.value,
            password: event.target.password.value
        }
        fetch('http://localhost:4000/user/login', {
            method: "POST",
            mode: "cors",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(res => res.json())
        .then(res => {
            localStorage.setItem('authorization', `Bearer ${res.token}`);
            this.props.history.push("/editor");
        })
        .catch(err => console.error(err)) 
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-6">
                    <form name="login" onSubmit={this.handleSubmit}>
                        <div className="form-goup">
                            <label htmlFor="username">Username</label>
                        </div>
                        <div className="form-group">
                            <input className="form-control" type="text" name="username" />
                        </div>
                        <div className="form-goup">
                            <label htmlFor="password">Password</label>
                        </div>
                        <div className="form-goup">
                            <input className="form-control" type="password" name="password" />
                        </div>
                        <div className="form-group">
                        <button type="submit" className="btn-primary">Login</button>
                        </div>
                    </form>
                </div>
                <div className="col-md-6">
                    
                </div>
                <div className="col-md-6">
                    
                </div>
            </div>
            
        )
            
    }
}

export default Login;