import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import parser, { Tag } from 'bbcode-to-react';
import CodeTag from '../bbTags/codetag';

class Single extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            post: [],
            postBody: ''
        };
    }

    componentDidMount() {
        console.log(this.props.match.params)
        const postId = this.props.match.params.id;
        fetch(`http://localhost:4000/blog/${postId}`, {
                method: 'GET',
                mode: 'cors'
        })
        .then(res => res.json())
        .then(
            (res) => {
            this.setState({
                isLoaded: true,
                post: res,
                postBody: res.postBody
            });
            console.log(res);
        },
        (error) => {
            this.setState({
                isLoaded: true,
                error
            });
        });
    }

    componentDidUpdate() {
        console.log(this.state.post.postBody);
    }
    

    render() {
        const { error, isLoaded, post } = this.state;
        //const postBody = this.state.post.postBody;
        console.log(this.state.postBody);
        if(error) {
        return <div>Error: {error.message}</div>
        }
        return(
            <div className="single-post">
                <h1>
                    {post.title}
                </h1>
                <p className="author">
                    Author: {post.author}
                </p>
                <div className="post-body">
                {parser.toReact(this.state.postBody)}
                </div>
            </div>
            
        )
    }
}

export default Single;