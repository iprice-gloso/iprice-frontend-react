import React from "react";
import { Route, Redirect } from "react-router-dom";
import checkAuth from '../auth/checkAuth';

export const ProtectedRoute = ({ component: Component, ...rest }) => {
    return (
        <Route 
            {...rest}
            render={props => {
                const check = checkAuth.isAuthenticated();
                check.then(res => {
                    if(res.authenticated === true) {
                        return <Component {...props} />
                    } else {
                        return <Redirect to={
                            {
                                pathname: "/",
                                state: {
                                    from: props.location
                                }
                            }
                        } />
                    }
                })
                .catch(err => console.error(err))
                
            }}
        />
    )
}